import { Component } from '@angular/core';

export interface List {
  text: string;
  }



@Component({
  selector: 'ed-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /*title = 'new-proyect';*/

  grupo1: List[] = [
    {
      text: 'Test accordion'
    },
    {
      text: 'A Brief History of Spaceflight'
        }
  ];


  grupo2: List[] = [
    {
      text: 'Safety Symbols'
    }
  ];

  grupo3: List[] = [
    {
      text: 'Command Center'
    },
    {
      text: 'Guidance System'
        },
        {
          text: 'Ground Segment'
            }
  ];


  grupo4: List[] = [
    {
      text: 'Building a Mars Rover'
    },
    {
      text: 'Building a Space Ship'
        },
  ];

}

